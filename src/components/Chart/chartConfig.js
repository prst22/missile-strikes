const font = {
    size: 16,
    family: 'Segoe UI, Roboto'
}

export const chartConfig = {
    responsive: true,
    maintainAspectRatio: false,
    animation: {
        duration: 700
    },
    hoverRadius: 6,
    scales: {
        y: {
            suggestedMin: 1,
            suggestedMax: 100,
            grid: {
                color: '#303030'
            },
            ticks: {
                font: {
                    size: 14,
                    family: 'Segoe UI, Roboto'
                },
                color: '#919191',
                precision: 0,
                beginAtZero: true,
            },
            title: {
                display: true,
                text: 'QUANTITY'
            },
        },
        x: {
            grid: {
                color: '#303030'
            },
            ticks: {
                font: {
                    size: 12,
                    family: 'Segoe UI, Roboto'
                },
                color: '#919191'
            },
            title: {
                display: true,
                text: 'DATE D/M/Y'
            }
        }
    },
    plugins: {
        legend: {
            labels: {
                usePointStyle: true,
                font: {
                    size: 14
                }
            },
        },
        tooltip: {
            bodyFont: {
                size: 14,
                family: 'Segoe UI, Roboto'
            },
            callbacks: {
                afterBody: function(data) {
                    let str = '',
                    allData = data[0].dataset.data,
                    index = data[0].dataIndex,
                    types = allData[index].type

                    if (allData[index].total_number !== 0 
                        && allData[index].total_number !== null) {
                        str += 'Total strikes: '
                        str += allData[index].total_number
                        str += ' \n'
                    } else if (allData[index].y > 0 && allData[index].total_number === 0) {
                        str += 'Total strikes: unknown \n'
                    } else if (allData[index].y === 0 && allData[index].total_number === 0) {
                        str += 'Total strikes: 0'
                    } else if (allData[index].y === 0 && allData[index].total_number === null) {
                        str += 'Total strikes: 0'
                    } else if (allData[index].y > 0 && allData[index].total_number === null) {
                        str += 'Total strikes: unknown \n'
                    }
                  
                    if (allData[index].y > 0 && types.length > 0) {
                        str += 'Type: '
                        let weaponTypesStr = types.join(', ')
                        str += weaponTypesStr
                    } else if (allData[index].y > 0 && types.length <= 0){
                        str += 'Type: unknown'
                    }

                    return str
                },

            }
        }
    }
}