export function prepareChatData(jsonData) {
    let data = {
        datasets: [
            {
                label: 'Downed Missile',
                data: [],
                pointRadius: 3,
                elements: {
                    point: {
                        backgroundColor: '#FB3640',
                    },
                    line: {
                        borderColor: '#FB3640',
                        borderJoinStyle: 'round',
                        borderWidth: 2
                    },
                },
            },
            {
                label: 'Downed Drone',
                data: [],
                pointRadius: 3,
                elements: {
                    point: {
                        backgroundColor: '#2292a4',
                    },
                    line: {
                        borderColor: '#2292a4',
                        borderJoinStyle: 'round',
                        borderWidth: 2
                    },
                },
            },
            {
                label: 'Downed Missile and Drone',
                data: [],
                pointRadius: 3,
                hidden: true,
                elements: {
                    point: {
                        backgroundColor: '#f58f29',
                    },
                    line: {
                        borderColor: '#f58f29',
                        borderJoinStyle: 'round',
                        borderWidth: 2
                    },
                },
            },
        ]
    }

    jsonData.forEach(item => {
        let droneTotalDestroyed = item.drone.total_destroyed === null ? 0 : item.drone.total_destroyed,
            missileTotalDestroyed = item.missile.total_destroyed === null ? 0 : item.missile.total_destroyed,
            droneTotalLaunched = item.drone.total_number,
            missileTotalLaunched = item.missile.total_number

        
        // TOTAL MISSILES DESTROYED DATASET DATA
        data.datasets[0].data.push({
            x: item.date,
            y: missileTotalDestroyed,
            total_number: item.missile.total_number,
            type: item.missile.type,
            related_video: item.related_video,
		    description: item.description,
        })
        // TOTAL DRONES DESTROYED DATASET DATA
        data.datasets[1].data.push({
            x: item.date,
            y: droneTotalDestroyed,
            total_number: item.drone.total_number,
            type: item.drone.type,
            related_video: item.related_video,
            description: item.description,
        })
        // TOTAL STRIKES DESTROYED DATASET DATA
        let totalStrikes = null

        if (Number.isInteger(droneTotalLaunched) && Number.isInteger(missileTotalLaunched)) {
            totalStrikes = droneTotalLaunched + missileTotalLaunched
        }
        
        data.datasets[2].data.push({
            x: item.date,
            y: droneTotalDestroyed + missileTotalDestroyed,
            total_number: totalStrikes,
            type: item.missile.type.concat(item.drone.type),
            related_video: item.related_video,
		    description: item.description,
        })
    })

    return data
}